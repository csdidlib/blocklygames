moveForward();
moveForward();
turnLeft();
moveForward();
while (notDone()) {
  if (isPathLeft()) {
    turnLeft();
  }
  if (isPathForward()) {
    moveForward();
  } else {
    turnRight();
  }
}
