penColour('#ff0000');
if (time() < 50) {
  circle(50, 70, 10);
} else {
  circle(50, 80, 20);
}
penColour('#3333ff');
rect(50, 40, 20, 40);
penColour('#999999');
line(40, 50, 20, 100 - time(), 5);
line(60, 50, 80, Math.pow((time() - 50) / 5, 2), 5);
penColour('#009900');
line(40, 20, time(), 0, 5);
line(60, 20, 100 - time(), 0, 5);
penColour('#ff0000');
circle(80, Math.pow((time() - 50) / 5, 2), 5);
circle(20, 100 - time(), 5);
