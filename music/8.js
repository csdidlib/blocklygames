function start1() {
  setInstrument('violin');
  play2();
  play2();
}


function play2() {
  first_part();
  first_part();
  second_part();
  second_part();
  third_part();
  third_part();
  fourth_part();
  fourth_part();
}

function first_part() {
  play(0.25, 7);
  play(0.25, 8);
  play(0.25, 9);
  play(0.25, 7);
}

function fourth_part() {
  play(0.25, 7);
  play(0.25, 4);
  play(0.5, 7);
}

function second_part() {
  play(0.25, 9);
  play(0.25, 10);
  play(0.5, 11);
}

function third_part() {
  play(0.125, 11);
  play(0.125, 12);
  play(0.125, 11);
  play(0.125, 10);
  play(0.25, 9);
  play(0.25, 7);
}

function start2() {
  setInstrument('violin');
  rest2();
  play2();
  play2();
}


function rest2() {
  rest(1);
  rest(1);
}
